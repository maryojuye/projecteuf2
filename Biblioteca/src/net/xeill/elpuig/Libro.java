package net.xeill.elpuig;
class Libro {
  String nombre;
  int ano;
  String categoria;
  String autorname;
  boolean reservado;
  Autor autor;

  Libro(String n, int a, String autorname, String c,boolean reservado){
    this.nombre = n;
    this.ano = a;
    this.autorname = autorname;
    this.categoria = c;
    this.reservado = reservado;
  }

  public String toString() {
    String s = "";
    s= "Nombre: "+this.nombre+"\n";
    s+= "Año: "+this.ano+"\n";
    s+= "Autor: "+this.autorname+"\n";
    s+= "Categoria: "+this.categoria+"\n";
    s+= "reservado: "+this.reservado+"\n";
    return s;
  }
}
