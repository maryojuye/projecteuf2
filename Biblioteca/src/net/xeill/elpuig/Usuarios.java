package net.xeill.elpuig;

class Usuarios {
  String id;
  String nomycog;
  int librosreservados;

  Usuarios(String i, String n, int l){
    this.id = i;
    this.nomycog = n;
    this.librosreservados = l;
  }

  public String toString() {
    String u = "";
    u= "Id: "+this.id+"\n";
    u+= "Nombre y apellido: "+this.nomycog+"\n";
    u+= "Cantidad de libros reservados: "+this.librosreservados+"\n";
    return u;
  }
}
