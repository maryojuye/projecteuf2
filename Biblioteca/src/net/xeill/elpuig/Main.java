package net.xeill.elpuig;
import java.util.*;
import java.io.*;

class Main {
  public static void main(String[] args) {

    Biblioteca biblioteca = new Biblioteca();
    biblioteca.autores();
    //biblioteca.Usuarioscargados();
    biblioteca.inicializarReserva(new File("net/xeill/elpuig/reservas.data"));
    biblioteca.inicializarUsuarios(new File("net/xeill/elpuig/usuarios.data"));
    biblioteca.inicializarLibros(new File("net/xeill/elpuig/libro.data"));

    Menu menu = new Menu();

    while (true) {
      // Muestra el menú
      menu.showMainMenu();
      // Retorna la opción elegida en el menú
      int option = menu.getMainMenu();
      //System.out.println(option);

      switch (option) {
        case 1:
        biblioteca.areaLibros();
          break;
        case 2:
        biblioteca.busquedaAutores();
          break;
        case 3:
        biblioteca.mirarReservado();
          break;
        case 4:
        biblioteca.addReservado();
        biblioteca.guardarReserva(new File("net/xeill/elpuig/reservas.data"));
        biblioteca.guardarUsuarios(new File("net/xeill/elpuig/usuarios.data"));
        biblioteca.guardarLibros(new File("net/xeill/elpuig/libro.data"));

          break;
        case 5:
        biblioteca.eliminarReserva();
        biblioteca.guardarReserva(new File("net/xeill/elpuig/reservas.data"));
        biblioteca.guardarUsuarios(new File("net/xeill/elpuig/usuarios.data"));
        biblioteca.guardarLibros(new File("net/xeill/elpuig/libro.data"));
          break;
        case 6:
        biblioteca.areaUsuarios();
          break;
        case 7:
        System.out.println("Saliendo...");
        System.exit(0);
          break;
        default: break;
      }
    }

  }
}
