package net.xeill.elpuig;
import java.util.Scanner;

class Menu {

  Scanner scanner = new Scanner(System.in);

  public void showMainMenu() {

    System.out.println("1. Area libros");
    System.out.println("2. Buscar libros segun autor");
    System.out.println("3. Mirar libro reservado");
    System.out.println("4. Añadir libro a reservado");
    System.out.println("5. Quitar libro de reserva");
    System.out.println("6. Area usuarios");
    System.out.println("7. Salir del programa");
  }

  public int getMainMenu() {
    String soption;
    int option;
    try {
      soption = scanner.nextLine();
      option = Integer.parseInt(soption);
    } catch (Exception e) {
      cls();
      showMainMenu();
       return getMainMenu();
    }

    return option;
  }

  // Limpia el terminal
  void cls() {
    System.out.print("\033\143");
  }
}
