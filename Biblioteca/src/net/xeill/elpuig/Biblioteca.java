package net.xeill.elpuig;
import java.time.*;
import java.util.*;
import java.io.*;
//asixdam1a@stallman-105:~$ javac net/xeill/elpuig/Main.java
//java net.xeill.elpuig.Main
class Biblioteca {
  Autor autores = new Autor();
  ArrayList<Libro> libros = new ArrayList<Libro>();
  ArrayList<Reservado> reservas = new ArrayList<Reservado>();
  ArrayList<Usuarios> usuarios = new ArrayList<Usuarios>();


  Scanner scanner = new Scanner(System.in);

  public void muestraLibros()  {
    for (Libro s : libros) {
      System.out.println("                \uD83D\uDCD6 LIBRO              ");
      System.out.println("--------------------------------------------");
      System.out.println(s);
      System.out.println("--------------------------------------------");
    }

  }

    public void showLibrosMenu() {
      int i = 1;
      for (Libro s : libros) {
        System.out.println("\uD83D\uDFE1"+" "+i+". "+s);
        i++;
      }
    }

    public int getLibrosMenu() {
      String soption;
      int option;
      try {
        soption = scanner.nextLine();
        option = Integer.parseInt(soption);
      } catch (Exception e) {
        cls();
        showLibrosMenu();
         return getLibrosMenu();
      }

      return option;
    }
    public void ReservadoMenu() {
      int i = 1;
      for (Reservado r : reservas) {
        System.out.println("\uD83D\uDFE1"+" "+i+". "+r);
        i++;
      }
    }

    public int getReservadosMenu() {
      String soption;
      int option;
      try {
        soption = scanner.nextLine();
        option = Integer.parseInt(soption);
      } catch (Exception e) {
        cls();
        ReservadoMenu();
         return getReservadosMenu();
      }

      return option;
    }

    public void muestraUsuarios()  {
      for (Usuarios u : usuarios) {
        System.out.println("                \uD83D\uDC64 USUARIO              ");
        System.out.println("--------------------------------------------");
        System.out.println(u);
        System.out.println("--------------------------------------------");
      }

    }

    public void showUsuariosMenu() {
      int i = 1;
      for (Usuarios u : usuarios) {
        System.out.println(i+". "+u);
        i++;
      }
    }

    public void UsuariosMenu() {
      for (Usuarios u : usuarios) {

      }
    }

    public int getUsuariosMenu() {
      String soption;
      int option;
      try {
        soption = scanner.nextLine();
        option = Integer.parseInt(soption);
      } catch (Exception e) {
        cls();
        showUsuariosMenu();
         return getUsuariosMenu();
      }

      return option;
    }
    public void areaLibros(){
      System.out.println("Escoge lo que quieres hacer:");
      System.out.println("1. Mostrar libros.");
      System.out.println("2. Añadir libros.");
      //System.out.println("3. Eliminar usuarios.");
      int opcion = scanner.nextInt();
      scanner.nextLine();
      if (opcion == 1){
        this.muestraLibros();
      }else if (opcion == 2){
        this.addLibros();
      }//else if (opcion == 3){
        //this.deleteUsuarios();
      //}

    }

  public void addLibros(){
    System.out.println("Escribe el nombre del nuevo libro: ");
    String nombre = scanner.nextLine();
    System.out.println("Escriba el año de la publicación del libro");
    int year = scanner.nextInt();
    scanner.nextLine();
    System.out.println("Escriba el nombre del autor/a: ");
    String autor = scanner.nextLine();
    System.out.println("Escriba la categoria del libro: ");
    String categoria = scanner.nextLine();
    Libro libro = new Libro(nombre,year,autor,categoria,false);
    this.libros.add(libro);
    autores.nombrea.add(autor);
    this.guardarLibros(new File("net/xeill/elpuig/libro.data"));
  }

  public void busquedaAutores()  {
    System.out.println("Ecribe el autor que quieres buscar: ");
    String busqueda = scanner.nextLine();
    boolean comprobar = false;
    for(int i=0; i < autores.nombrea.size(); i++){
      if (busqueda.equals(autores.nombrea.get(i)) ){
        comprobar = true;

      }
    }
    if (comprobar == true){
      System.out.println(ConsoleColors.INVENTADO +"Autor/a disponible"+
    	ConsoleColors.RESET);
      System.out.println("Los libros disponibles para este autor/a son: ");
      boolean tiene = false;
      for (Libro s : libros) {
        if (s.autorname.equals(busqueda)){
          tiene = true;
          System.out.println("\u27A1\uFE0F"+" "+s.nombre+"--"+s.categoria);
          }
        }
        if (tiene == false){
            System.out.println(ConsoleColors.RED_BOLD+"No tenemos libros de este autor, intentelo mas tarde."
            +ConsoleColors.RESET);
      }
      System.out.println("--------------------------------------------");

    }else if (comprobar == false){
      System.out.println(ConsoleColors.RED_BOLD +"Autor/a no disponible"+
    	ConsoleColors.RESET);
    }
  }

  public void mirarReservado(){
    System.out.println("Ecribe el libro que quieres buscar: ");
    String busqueda2 = scanner.nextLine();
    boolean prueba = false;
    for (Reservado r : reservas){
    if (busqueda2.equals(r.libro)){
        System.out.println(ConsoleColors.RED_BOLD+"Libro reservado"+ConsoleColors.RESET);
        System.out.println(r);
        prueba = true;
      }
    }
    if (prueba == false){
     System.out.println(ConsoleColors.GREEN_BOLD+"Este libro no está reservado"+ConsoleColors.RESET);
   }
  }

public void addReservado(){
  cls();
  System.out.println("Selecciona el libro a reservar: ");
  try {
    this.showLibrosMenu();
    //this.ReservadoMenu();
    int option = this.getLibrosMenu();
    Libro s = this.libros.get(option - 1);
    System.out.println("----------------------------------");
    System.out.println(s);
    if (s.reservado == false){
      System.out.println(ConsoleColors.GREEN_BOLD+"Quieres reservar este libro(s/n)?"+ConsoleColors.RESET);
      String cambiaReservado = scanner.nextLine();

      if (cambiaReservado.equals("s")) {
        System.out.println(ConsoleColors.GREEN_BOLD+"Elige el usuario que va a reservar el libro"+ConsoleColors.RESET);
        this.showUsuariosMenu();
        int optionu = this.getUsuariosMenu();
        Usuarios u = this.usuarios.get(optionu - 1);
        System.out.println(ConsoleColors.GREEN_BOLD+"Introduce true, si quieres que este libro sea reservado"+ConsoleColors.RESET);
        boolean nuevaReserva = scanner.nextBoolean();
        Reservado reserva = new Reservado(s.nombre,u.nomycog,LocalDate.now(),LocalDate.now().plusDays(15));
        this.reservas.add(reserva);
        s.reservado = nuevaReserva;
        u.librosreservados++;
      }
    }else if (s.reservado == true){
        System.out.println(ConsoleColors.RED_BOLD+"Lo siento, pero este libro ya ha sido reservado, espera a que sea devuelto"+ConsoleColors.RESET);
    }


  } catch (Exception e) {
    cls();
    addReservado();
  }
}

public void eliminarReserva() {
  if (reservas.size() == 0) {
    System.out.println(ConsoleColors.RED_BOLD+"No hay reservas disponibles"+ConsoleColors.RESET);
  }else if (reservas.size() != 0){
    cls();
    System.out.println("Selecciona la reserva que quiere eliminar: ");
    this.ReservadoMenu();
    //this.UsuariosMenu();
    int option = this.getReservadosMenu();
    Reservado r = this.reservas.get(option - 1);
    System.out.println("Si quieres cancelar la operación pon: c, sino pon: s ");
    String comprobar = scanner.nextLine();
    if (comprobar.equals("s")){
      for (Usuarios u : usuarios) {
        if (u.nomycog.equals(r.usuario)){
          u.librosreservados--;
        }
      }
      for (Libro l : libros) {
        if (l.nombre.equals(r.libro)){
          l.reservado=false;
        }
      }
        this.reservas.remove(option - 1);

      }

    }

}

public void addUsuarios(){

  int totalUsuarios = usuarios.size();
  System.out.println("Este es el numero total de usuarios que hay inscritos:"+totalUsuarios+" ,escriba un numero mayor");
  System.out.println("Id que tendrá el usuario: ");
  String id = scanner.nextLine();
  int num1 = Integer.parseInt(id);
  if (num1 > totalUsuarios){
    System.out.println("Escribe el nombre y apellido del nuevo usuario: ");
    String nomycog = scanner.nextLine();
    System.out.println("Como el usuario es nuevo, empezara con 0 libros reservados, pon: 0");
      int librosreservados = scanner.nextInt();
      scanner.nextLine();
    Usuarios usuario = new Usuarios(id,nomycog,librosreservados);
    this.usuarios.add(usuario);
    this.guardarUsuarios(new File("net/xeill/elpuig/usuarios.data"));
  }else if (num1 <= totalUsuarios){
    System.out.println(ConsoleColors.RED_BOLD+"El numero de id que ha escrito ya existe, escriba otro"+ConsoleColors.RESET);
  }

}

public void deleteUsuarios(){
  cls();
  System.out.println("Selecciona el usuario que quiere eliminar: ");
  this.showUsuariosMenu();
  int option = this.getUsuariosMenu();
  Usuarios u = this.usuarios.get(option - 1);
  System.out.println("Si quieres cancelar la operación pon: c, sino pon: s ");
  String comprobar = scanner.nextLine();
  if (comprobar.equals("s")){

    //
    int i = 0;
    int guardar = 0;
    for (Reservado r : reservas) {
      if (r.usuario.equals(u.nomycog)) {
        guardar = i;
      }
      i++;
    }
    reservas.remove(guardar);
    this.usuarios.remove(option - 1);

    this.guardarUsuarios(new File("net/xeill/elpuig/usuarios.data"));
    this.guardarReserva(new File("net/xeill/elpuig/reservas.data"));

  }
}

public void areaUsuarios(){
  System.out.println("Escoge lo que quieres hacer:");
  System.out.println("1. Mostrar usuarios.");
  System.out.println("2. Añadir usuarios.");
  System.out.println("3. Eliminar usuarios.");
  int opcion = scanner.nextInt();
  scanner.nextLine();
  if (opcion == 1){
    this.muestraUsuarios();
  }else if (opcion == 2){
    this.addUsuarios();
  }else if (opcion == 3){
    this.deleteUsuarios();
  }

}


public void autores(){
  autores.nombrea.add("Cercas,Javier");
  autores.nombrea.add("Marías,Javier");
  autores.nombrea.add("J.K. Rowling");
  autores.nombrea.add("J.R.R. Tolkien");
  autores.nombrea.add("Paulo Coelho");
  autores.nombrea.add("Dan Brown");
  autores.nombrea.add("Stephenie Meyer");
  autores.nombrea.add("Margaret Mitchell");
  autores.nombrea.add("Napoleón Hill");
  autores.nombrea.add("Anna Frank");
  autores.nombrea.add("Blue Jeans");
  autores.nombrea.add("Charles Dickens");
  autores.nombrea.add("Cao Xueqin");
  autores.nombrea.add("Lewis Carroll");
  }


//INICIO GUARDAR RESERVAS
  void guardarReserva(File file) {
  try {
    BufferedWriter outputStream = new BufferedWriter(new FileWriter(file, false)); // NO append

    for (Reservado r : reservas) {
      outputStream.write(r.libro+":"+r.usuario+":"+r.fecha+":"+r.fechav+"\n");
    }

    outputStream.close();

  } catch(Exception e) {
    System.out.println(e.getMessage());
  }
}
void inicializarReserva(File file) {

  try {
    BufferedReader inputStream = new BufferedReader(new FileReader(file));
    String line = "";

    while((line = inputStream.readLine()) != null) {
      //System.out.println(line);
      String[] parts = line.split(":");
      Reservado r = new Reservado(parts[0], parts[1], LocalDate.parse(parts[2]), LocalDate.parse(parts[3]));
      for (Libro l : libros) {
        if (l.nombre.equals(parts[0])){
          l.reservado=true;
        }
      }
      reservas.add(r);
    }

    inputStream.close();

  } catch(Exception e) {
    System.out.println(e.getMessage());
  }

}
//FIN GUARDAR RESERVAS

//INICIO GUARDAR USUARIOS
void guardarUsuarios(File file) {
try {
  BufferedWriter outputStream = new BufferedWriter(new FileWriter(file, false)); // NO append

  for (Usuarios u : usuarios) {
    outputStream.write(u.id+":"+u.nomycog+":"+u.librosreservados+"\n");
  }

  outputStream.close();

} catch(Exception e) {
  System.out.println(e.getMessage());
}
}
void inicializarUsuarios(File file) {

try {
  BufferedReader inputStream = new BufferedReader(new FileReader(file));
  String line = "";

  while((line = inputStream.readLine()) != null) {
    String[] parts = line.split(":");
    Usuarios u = new Usuarios(parts[0], parts[1], Integer.parseInt(parts[2]));
    usuarios.add(u);
  }

  inputStream.close();

} catch(Exception e) {
  System.out.println(e.getMessage());
}

}
//FIN GUARDAR USUARIOS

//INICIO GUARDAR LIBROS
void guardarLibros(File file) {
try {
  BufferedWriter outputStream = new BufferedWriter(new FileWriter(file, false)); // NO append

  for (Libro l : libros) {
    outputStream.write(l.nombre+":"+l.ano+":"+l.autorname+":"+l.categoria+":"+l.reservado+"\n");
  }

  outputStream.close();

} catch(Exception e) {
  System.out.println(e.getMessage());
}
}
void inicializarLibros(File file) {

try {
  BufferedReader inputStream = new BufferedReader(new FileReader(file));
  String line = "";

  while((line = inputStream.readLine()) != null) {
    String[] parts = line.split(":");
    Libro l = new Libro(parts[0],Integer.parseInt(parts[1]),parts[2],parts[3],Boolean.parseBoolean(parts[4]));
    libros.add(l);
  }

  inputStream.close();

} catch(Exception e) {
  System.out.println(e.getMessage());
}

}
//FIN GUARDAR LIBROS





  void cls() {
    System.out.print("\033\143");
  }
}
