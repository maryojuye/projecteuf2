package net.xeill.elpuig;
import java.util.*;
import java.time.*;


class Reservado {
  String libro;
  String usuario;
  LocalDate fecha;
  LocalDate fechav;
  Usuarios usuarios;

  Reservado(String l, String u, LocalDate f, LocalDate v){
    this.libro = l;
    this.usuario = u;
    this.fecha = f;
    this.fechav = v;
  }
  public String toString() {
    String r = "";
    r= "Libro: "+this.libro+"\n";
    r+= "Usuario: "+this.usuario+"\n";
    r+= "Fecha: "+this.fecha+"\n";
    r+= "Devolución: "+this.fechav+"\n";
    return r;
  }

}
